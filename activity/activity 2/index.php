<?php require_once "./code.php" ?>
<!-- php -S localhost:8000 -->
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ACTIVITY 2</title>
</head>
<body>
	<h1>Letter-Based Grading</h1>
	<p>87 is equivalent to <?php echo getLetterGrade(87); ?></p>
	<p>94 is equivalent to <?php echo getLetterGrade(94); ?></p>
	<p>74 is equivalent to <?php echo getLetterGrade(74); ?></p>
</body>
</html>