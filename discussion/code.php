<?php
 // php -S localhost:8000 

/*$trial = 'Hello World';*/

//[Section] Comments
// Comments are part of the code that gets ignored by the language
// Comments are ment to describe the algorithm of the written code
/*
	There are two types of comments:
		- The single-line comment denoted by two forward slashes (//)
		- The multi-line comment denoted by a slash and asterisk (/* )
*/

//[Section] Variables
// Variales are used to contain data
// Variables are named location for stored values
// Variables are defined using the dollar ($) notation before the name of the variable


$name = 'John Smith';
$email = 'JohnSmith@email.com';

//[Section] Constants
// Constants are used to hold data that are meant to be read-only
// Constants are defined using define() function
	/*
		Syntax:
			define ('variableName', valueOfTheVariable)

	*/

define ('PI', 3.141592658979);


// Reassignment of variables
$name = "Will Smith";

//[Section] Data Types
//Strings
$states = 'New York';
$country = 'United States of America';
//concatinate using dot (.)
$address = $states.', '.$country.', '.PI;


//reassigning the address
//$address = "$states , $country";


//Integer
$age = 31;
$headcount = 26;

//Float
$grade = 98.2;
$distanceInKilometers = 3124.12;


//Boolean
$hasTraveledAbroad = false;
$hasSymptoms = true;


//Null
$spouse = null;
$middle = null;


//Array
$grades = array(98.7, 92.1, 90.2, 94.6);


//Object
$gradesObj = (object)[
	'firstGrading' => 98.7, 
	'secondGrading' => 92.1, 
	'thirdGrading' => 90.2, 
	'fourthGrading' =>94.6];

$personObj = (object)[
	'fullName' => 'John Smith', 
	'isMarried' => false, 
	'age' => 35, 
	'address' => (object)[
		'state' => 'New York',
		'country' => 'United States of America'
	],
	'contact' => array ('0912345645', '0124332543')

];


//[Section] Operators
//Assignment operators (=)

//This operator is used to assign and reaasign value/s of a variable

$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;


//[Section] Function
//Function are reusable code

function getFullName($firstName, $middleInitial, $lastName){


	return "$lastName, $firstName $middleInitial";
}


//[Section] Selection Control Structure
//Selection control structures are used to make cdode execute dynamic according to prediefined conditions

// IF-ELSEIF-ELSE condition

function determineTyphoonIntensity($windSpeed){

	if ($windSpeed <30){
		return 'Not a Typhoon yet.';
	}else if ($windSpeed <= 61){
		return 'Tropical depression Detected';
	}else if ($windSpeed <=88 ){
		return 'Tropical Storm Detected';
	}else if ($windSpeed <=17 ){
		return 'Severe Tropical Storm Detected';
	}else {
		return ' Typhoon Detected';
	}

}

// Conditional (Ternary ) Operator

function isUnderAge ($age){
	return ($age <18) ? true : false;
}

// Switch Statement
function determineComputerUser ($computerNumber){
	switch ($computerNumber) {
		case 1: 
			return 'Linux Torvalds';
			break;
		case 2: 
			return 'Steve Jobs';
			break;
		case 3: 
			return 'Sid Mier';
			break;
		case 4: 
			return 'Onel de Guzman';
			break;		
		case 5: 
			return 'Christian Salvador';
			break;	
		default:
			return $computerNumber. ' is out of bounds.';
			break;
	}
}

//Try-Catch-Finally Statement

function greeting ($str){
	try{
		if(gettype($str) == "string"){
			// if successful
			echo $str;
		} else {
			// if not, it will return "Oops!"
			throw new Exception ("Oops!");
		}
	} 
	catch (Exception $e){
		// Catch the error within "try"
		echo $e->getMessage();
	} 
	finally {
		// Continue execution of code regardless of success and failure of code execution in 'try' block
		echo " I did it again!";
	}
}

















?>