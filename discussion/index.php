<?php 
require_once "./code.php"




?>

<!-- php -S localhost:8000 -->

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S01: PHP Basics and Selection Control Structures</title>
</head>
<body>
	<h1>Echoing Values</h1>
	<!-- <p><?php echo "$trial"; ?></p> -->

	<!-- Using Single quote for the echo -->
	<p><?php echo 'Good Day $name! Your given email is $email'; ?></p>

	<!-- Using Double quote for the echo -->
	<p><?php echo "Good Day $name! Your given email is $email"; ?></p>


	<!-- Using constants -->
	<p><?php echo PI; ?></p>

	<p><?php echo $name; ?></p>	

	<h1>Data types</h1>
	<h3>Strings</h3>

	<p><?php echo $states; ?></p>	
	<p><?php echo $country; ?></p>	
	<p><?php echo $address; ?></p>	


	<h3>Integer</h3>
	<p><?php echo $age; ?></p>
	<p><?php echo $headcount; ?></p>

	<h3>Float</h3>
	<p><?php echo $grade; ?></p>
	<p><?php echo $distanceInKilometers; ?></p>

	<h3>Boolean</h3>
	<!-- Normal echoing of boolean variables will not make it to the webpage -->
	<p><?php echo var_dump($hasTraveledAbroad); ?></p>
	<p><?php echo var_dump($hasSymptoms); ?></p>


	<h3>Null</h3>
	<p><?php echo var_dump($spouse); ?></p>
	<p><?php echo var_dump($middle); ?></p>

	<h3>Arrays</h3>
	<p><?php echo "$grades[0] , $grades[1] , $grades[2] "; ?></p>
	<p><?php echo print_r($grades); ?></p>

	<h3>Objects</h3>
	<p><?php echo print_r($gradesObj); ?></p>
	<p><?php echo $gradesObj->firstGrading; ?></p>

	<p><?php echo print_r($personObj); ?></p>
	<p><?php echo $personObj->address->country; ?></p>
	<p><?php echo $personObj->contact[0]; ?></p>


	<h3>Gettype()</h3>
	<p><?php echo gettype($state); ?></p>
	<p><?php echo gettype($age); ?></p>
	<p><?php echo gettype($grade); ?></p>
	<p><?php echo gettype($hasTraveledAbroad); ?></p>
	<p><?php echo gettype($spouse); ?></p>
	<p><?php echo gettype($grades); ?></p>
	<p><?php echo gettype($gradesObj); ?></p>


	<h1>Operators</h1>
	<h3>Assignment Operators</h3>
	<p>X: <?php echo $x ?></p>
	<p>Y: <?php echo $y ?></p>

	<p>Is Legal Age: <?php echo var_dump($isLegalAge); ?></p>
	<p>Is Registered: <?php echo var_dump($isRegistered); ?></p>

	<h3>Arithmetic Operators</h3>
	<p>Sum: <?php echo $x + $y; ?></p>
	<p>Difference: <?php echo $x - $y; ?></p>
	<p>Product: <?php echo $x * $y; ?></p>
	<p>Quotients: <?php echo $x / $y; ?></p>

	<h3>Equality Operators</h3>
	<p>Loose Equality <?php echo var_dump($x == '1342.14')?></p>
	<p>Strict Equality <?php echo var_dump($x === '1342.14')?></p>


	<h3>Inequality Operators</h3>
	<p>Loose Inequality <?php echo var_dump($x != '1342.14') ?></p>
	<p>Strict Inequality <?php echo var_dump($x !== '1342.14')?></p>
	<!-- Strict equality/inequality is perferred so that you can check both of the value and the data type given -->

	<h3>Greater / Lesser Operators</h3>
	<p>Is Lesser <?php echo var_dump($x < $y) ?></p>
	<p>Is Greater <?php echo var_dump($x > $y)?></p>

	<p>Is Lesser or Equal <?php echo var_dump($x <= $y) ?></p>
	<p>Is Greater or Equal <?php echo var_dump($x >= $y)?></p>

	<h3>Logical Operators</h3>
	<!-- Logical Operation are used to verify whether an expression or group of expessions are true or false -->
	<p>OR Operator <?php echo var_dump($isLegalAge || $isRegistered) ?></p>
	<p>AND Operator <?php echo var_dump($isLegalAge && $isRegistered) ?></p>
	<p>NOT Operator <?php echo var_dump(!$isLegalAge) ?></p>

	<h1>Functions</h1>
	<!-- only the fist 3 variables are considered -->
	<p><?php echo getFullName('John', 'D.', 'Smith', 'luffy') ?></p>

	<h3>IF-Else-If Statement</h3>
	<p><?php echo determineTyphoonIntensity(39) ?></p>

	<h2>Ternary Sample (Is Underage?)</h2>
	<p>78: <?php echo var_dump(isUnderage(78)) ?></p>
	<!-- no echo -->
	<!-- if you use var_dump you dont have to use echo -->
	<p>17: <?php var_dump(isUnderage(17)) ?></p>

	<h2>Switch Operators</h2>
	<p><?php echo determineComputerUser(5) ?></p>
	<p><?php echo determineComputerUser(7) ?></p>

	<h2>Try-Catch-Finally</h2>
	<p><?php echo greeting(12) ?></p>
	<p><?php echo greeting("Hello") ?></p>


















</body>
</html>
